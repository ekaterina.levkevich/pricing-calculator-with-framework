package google_cloud;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GoogleCloudCalculatorPage extends AbstractPage {
    private String BASE_URL = "https://cloud.google.com/products/calculator?hl=ru";
    @FindBy (xpath = "//span[@jsname='m9ZlFb']/ancestor::button[1]")
    WebElement addToEstimateButton;
    public GoogleCloudCalculatorPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void addToEstimate() {
        waitToBeClickable(addToEstimateButton).click();
    }

    public void chooseTool(String toolName) {
        String toolNameButtonXPath = "//h2[text()='" + toolName + "']/ancestor::div[1]";
        waitToBeClickable(toolNameButtonXPath).click();
    }
    @Override
    protected AbstractPage openPage() {
        driver.navigate().to(BASE_URL);
        return this;
    }

}
