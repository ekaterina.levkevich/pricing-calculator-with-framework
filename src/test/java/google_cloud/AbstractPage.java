package google_cloud;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public abstract class AbstractPage
{
    protected WebDriver driver;

    protected abstract AbstractPage openPage();

    protected AbstractPage(WebDriver driver)
    {
        this.driver = driver;
    }
    protected WebElement getElementByXPath(String xPath) {
        return driver.findElement(new By.ByXPath(xPath));
    }

    protected WebElement waitToBeClickable(String xPath) {
        By locator = new By.ByXPath(xPath);
        ExpectedCondition<WebElement> condition = ExpectedConditions.elementToBeClickable(locator);
        return new WebDriverWait(driver, Duration.of(10, ChronoUnit.SECONDS)).until(condition);
    }
    protected WebElement waitToBeClickable(WebElement webElement) {
        ExpectedCondition<WebElement> condition = ExpectedConditions.elementToBeClickable(webElement);
        return new WebDriverWait(driver, Duration.of(10, ChronoUnit.SECONDS)).until(condition);
    }
}
