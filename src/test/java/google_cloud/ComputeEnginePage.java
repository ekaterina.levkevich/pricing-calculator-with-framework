package google_cloud;

import model.MachineParams;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class ComputeEnginePage extends AbstractPage {
    private final String BASE_URL = "https://cloud.google.com/products/calculator?hl=ru&dl=CiQ5MDYyMjg3Yy03MDI3LTQ2NzgtOTUyZC1iYWFiZGI3MDQyNmUQCBokMkQ3NzczNjItMUNGMy00MDA3LTlFNTItODREMTM3REEyNDA1";
    @FindBy (xpath = "//div[@jsname='nd797b']/descendant::input[@value]")
    private WebElement numberOfInstanceField;
    @FindBy (xpath = "//span[text()='Operating System / Software']/ancestor::div[@jsname='oYxtQd']")
    private WebElement operatingSystemField;
    @FindBy (xpath = "//span[text()='Machine Family']/ancestor::div[@jsname='oYxtQd']")
    private WebElement machineFamilyField;
    @FindBy (xpath = "//span[text()='Series']/ancestor::div[@jsname='oYxtQd']")
    private WebElement seriesField;
    @FindBy (xpath = "//span[text()='Machine type']/ancestor::div[@jsname='oYxtQd']")
    private WebElement machineTypeField;
    @FindBy (xpath = "//button[@aria-label='Add GPUs']")
    private WebElement addGPUsButton;
    @FindBy (xpath = "//span[text()='GPU Model']/ancestor::div[@jsname='oYxtQd']")
    private WebElement gpuModelField;
    @FindBy (xpath = "//span[text()='Number of GPUs']/ancestor::div[@jsname='oYxtQd']")
    private WebElement numberOfGPUsField;
    @FindBy (xpath = "//span[text()='Local SSD']/ancestor::div[@jsname='oYxtQd']")
    private WebElement localSSDField;
    @FindBy (xpath = "//span[text()='Region']/ancestor::div[@jsname='oYxtQd']")
    private WebElement datacenterLocationField;
    @FindBy (xpath = "//div[text()='Estimated cost']/following::label")
    private WebElement estimatedCostField;
    @FindBy (xpath = "//button[@aria-label='Open Share Estimate dialog']")
    private WebElement shareButton;
    @FindBy (xpath = "//a[text()='Open estimate summary']")
    private WebElement openEstimateSummary;

    public ComputeEnginePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    private void setNumberOfInstance(String numberOfInstance) {
        waitToBeClickable(numberOfInstanceField).clear();
        numberOfInstanceField.sendKeys(numberOfInstance);
    }

    public void setParams(MachineParams params) {
        setNumberOfInstance(params.numberOfInstance());
        setOperatingSystem(params.operatingSystem());
        setProvisioningModel(params.provisioningModel());
        setMachineFamily(params.machineFamily());
        setSeries(params.series());
        setMachineType(params.machineType());
        switchAddGPUsButtonState();
        setGPUType(params.gpuType());
        setNumberOfGPUs(params.numberOfGPUs());
        setLocalSSD(params.localSSD());
        setDatacenterLocation(params.datacenterLocation());
        setCommittedUsage(params.committedUsage());
    }

    private void setOperatingSystem(String operatingSystem) {
        String operatingSystemVariantXPath =
                "//span[text()='" + operatingSystem +"']/ancestor::li";
        operatingSystemField.click();
        waitToBeClickable(operatingSystemVariantXPath).click();
    }

    public void setProvisioningModel(String provisioningModel) {
        String provisioningModelXPath = "//label[text()='" + provisioningModel + "']/parent::div";
        getElementByXPath(provisioningModelXPath).click();
    }

    private void setMachineFamily(String machineFamilyType) {
        String machineFamilyTypeXPath = "//span[text()='" + machineFamilyType + "']/ancestor::li";
        machineFamilyField.click();
        getElementByXPath(machineFamilyTypeXPath).click();
    }

    private void setSeries(String series) {
        String seriesTypeXPath = "//span[text()='" + series + "']/ancestor::li";
        seriesField.click();
        getElementByXPath(seriesTypeXPath).click();
    }

    private void setMachineType(String machineType) {
        String machineTypeXPath = "//span[text()='" + machineType + "']/ancestor::li";
        machineTypeField.click();
        getElementByXPath(machineTypeXPath).click();
    }

    //This button turn off by default
    public void switchAddGPUsButtonState() {
        addGPUsButton.click();
    }

    private void setGPUType(String gpuType) {
        String gpuTypeXPath = "//span[text()='" + gpuType + "']/ancestor::li";
        waitToBeClickable(gpuModelField).click();
        getElementByXPath(gpuTypeXPath).click();
    }

    private void setNumberOfGPUs(String numberOfGPUs) {
        String numberOfGPUsXPath = "//span[text()='" + numberOfGPUs + "']/ancestor::li";
        numberOfGPUsField.click();
        getElementByXPath(numberOfGPUsXPath).click();
    }

    private void setLocalSSD(String localSSD) {
        String localSSDXPath = "//span[text()='" + localSSD + "']/ancestor::li";
        localSSDField.click();
        getElementByXPath(localSSDXPath).click();
    }

    private void setDatacenterLocation(String datacenterLocation) {
        String datacenterLocationXPath = "//span[text()='" + datacenterLocation + "']/ancestor::li";
        datacenterLocationField.click();
        getElementByXPath(datacenterLocationXPath).click();
    }

    private void setCommittedUsage(String committedUsage) {
        String committedUsageXPath = "//label[text()='" + committedUsage + "']/parent::div";
        getElementByXPath(committedUsageXPath).click();
    }

    public String getEstimatedCostField() throws InterruptedException {
        /*Of course, using 'Thread.sleep' is a bad practice, but here it is the best solution*/
        Thread.sleep(5000);
        return estimatedCostField.getText();
    }

    public void clickShareButton() {
        waitToBeClickable(shareButton).click();
    }


    public String getOpenEstimateSummaryLink() {
        String openEstimateSummaryLinkXPath = "//a[@track-name='open estimate summary']";
        return getElementByXPath(openEstimateSummaryLinkXPath).getAttribute("href");
    }

    @Override
    protected AbstractPage openPage() {
        driver.navigate().to(BASE_URL);
        return this;
    }
}
