package google_cloud;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchResultPage extends AbstractPage{
    private String BASE_URL = "https://cloud.google.com/search?hl=ru&q=Google%20Cloud%20Platform%20Pricing%20Calculator";
    @FindBy(xpath = "//a[@href='https://cloud.google.com/products/calculator']")
    WebElement calculatorPageLink;
    public SearchResultPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void goToCalculatorPage() {
        waitToBeClickable(calculatorPageLink).click();
    }

    @Override
    protected AbstractPage openPage() {
        driver.navigate().to(BASE_URL);
        return this;
    }
}
