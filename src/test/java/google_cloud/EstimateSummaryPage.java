package google_cloud;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EstimateSummaryPage extends AbstractPage{
    String BASE_URL;
    @FindBy (xpath = "//h5[text() = 'Total estimated cost']/following-sibling::h4")
    WebElement totalEstimateCost;

    public EstimateSummaryPage(WebDriver driver, String estimateSummaryLink) {
        super(driver);
        BASE_URL = estimateSummaryLink;
        PageFactory.initElements(driver, this);
    }

    public String getTotalEstimatedCost() {
        return waitToBeClickable(totalEstimateCost).getText();
    }

    @Override
    public AbstractPage openPage() {
        driver.navigate().to(BASE_URL);
        return this;
    }

}
