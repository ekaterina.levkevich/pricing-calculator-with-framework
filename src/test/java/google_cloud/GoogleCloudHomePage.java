package google_cloud;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GoogleCloudHomePage extends AbstractPage{
    private String BASE_URL = "https://cloud.google.com/";
    @FindBy(xpath = "//form/descendant::input[@type='text']")
    WebElement searchField;
    @FindBy(xpath = "//form/descendant::i[@aria-label='Search']")
    WebElement searchButtonXPath;

    public GoogleCloudHomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void searchIconClick() {
        waitToBeClickable("//div[@class='YSM5S']").click();
    }

    public void setSearchField(String searchFieldFilling)  {
        searchField.sendKeys(searchFieldFilling);
    }

    public void search() {
        searchButtonXPath.click();
    }
    @Override
    public AbstractPage openPage() {
        driver.navigate().to(BASE_URL);
        return this;
    }
}
