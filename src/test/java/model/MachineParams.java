package model;

public record MachineParams(
        String numberOfInstance,
        String operatingSystem,
        String provisioningModel,
        String machineFamily,
        String series,
        String machineType,
        String gpuType,
        String numberOfGPUs,
        String localSSD,
        String datacenterLocation,
        String committedUsage
) {}
