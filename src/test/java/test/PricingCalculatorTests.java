package test;

import google_cloud.*;
import model.MachineParams;
import org.openqa.selenium.WindowType;
import org.testng.annotations.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;


public class PricingCalculatorTests extends CommonConditions {
    @Test
    public void computeEngineInstanceCreation() throws InterruptedException {
        GoogleCloudHomePage googleCloudHomePage = new GoogleCloudHomePage(driver);
        googleCloudHomePage.openPage();
        googleCloudHomePage.searchIconClick();
        googleCloudHomePage.setSearchField("Google Cloud Platform Pricing Calculator");
        googleCloudHomePage.search();

        SearchResultPage searchResultPage = new SearchResultPage(driver);
        searchResultPage.goToCalculatorPage();

        /*Due to absence of the 'COMPUTE ENGINE' on the current page the order of steps has been changed*/
        GoogleCloudCalculatorPage googleCloudCalculatorPage = new GoogleCloudCalculatorPage(driver);
        /*First 'Add to estimate' button press*/
        googleCloudCalculatorPage.addToEstimate();
        /*Then 'Compute Engine' select*/
        googleCloudCalculatorPage.chooseTool("Compute Engine");

        ComputeEnginePage computeEnginePage = new ComputeEnginePage(driver);
        MachineParams params = new MachineParams(
                "4",
                /*Since there is no 'What are these instances for?', appropriate step is missing.
                 * Due to absence the variant 'Free: Debian, CentOS, CoreOS, Ubuntu, or another User-Provided OS',
                 * the variant 'Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)' was selected
                 */
                "Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)",
                "Regular",
                "General Purpose",
                "N1",
                "n1-standard-8",
                "NVIDIA Tesla V100",
                "1",
                "2x375 GB",
                /*Due to absence the variant 'Frankfurt (europe-west3)' in the drop-down list,
                 * the 'Netherlands (europe-west4)' was selected
                 */
                "Netherlands (europe-west4)",
                "1 year"
        );
        computeEnginePage.setParams(params);
        String estimatedCostOnComputeEnginePage = computeEnginePage.getEstimatedCostField();
        /*$138.70 - the cost by default*/

        assertThat("$138.70", not(equalTo(computeEnginePage.getEstimatedCostField())));
        computeEnginePage.clickShareButton();
        String estimateSummaryLink = computeEnginePage.getOpenEstimateSummaryLink();

        driver.switchTo().newWindow(WindowType.TAB);
        EstimateSummaryPage estimateSummaryPage = new EstimateSummaryPage(driver, estimateSummaryLink);
        estimateSummaryPage.openPage();
        assertThat(estimatedCostOnComputeEnginePage, equalTo(estimateSummaryPage.getTotalEstimatedCost()));
    }
}
